import { shallowMount } from '@vue/test-utils'
import { Card } from 'element-ui'
import StatisticItem from '@/components/main/StatisticItem.vue'

describe('StatisticItem', () => {
  let wrapper = null
  beforeEach(() => {
    wrapper = shallowMount(StatisticItem, { components: { 'el-card': Card } })
  })

  it('is a Vue instance', () => {
    expect(wrapper).toBeTruthy()
  })

  it('has a title', () => {
    expect(wrapper.find('h3').text()).toEqual('Some title')
  })

  it('has a text in slot', () => {
    expect(wrapper.find('div[text="some position"]')).toBeTruthy()
  })
})
