import Vue from 'vue'
import Vuex from 'vuex'
import service from '../service'

Vue.use(Vuex)

const store = () => new Vuex.Store({
  state: {
    types: [],
    units: [],
    isLoading: false
  },

  mutations: {
    loadTypes: (state, payload) => (state.types = payload),
    loadUnits: (state, payload) => (state.units = payload),
    toggleLoading: (state, payload) => (state.isLoading = payload)
  },

  actions: {
    loadTypes: async ({ commit }) => {
      try {
        commit('toggleLoading', true)
        const types = await service.loadTypes()
        commit('loadTypes', types)
      } catch (err) {
      } finally {
        commit('toggleLoading', false)
      }
    },

    loadUnits: async ({ commit }) => {
      try {
        commit('toggleLoading', true)
        const units = await service.loadUnits()
        commit('loadUnits', units)
      } catch (err) {
      } finally {
        commit('toggleLoading', false)
      }
    },

    createType: async ({ commit, dispatch }, typeName) => {
      try {
        commit('toggleLoading', true)
        await service.createType(typeName)
        await dispatch('loadTypes')
      } catch (err) {
      } finally {
        commit('toggleLoading', false)
      }
    },

    createUnit: async ({ commit, dispatch }, unit) => {
      try {
        commit('toggleLoading', true)
        await service.createUnit(unit)
        await dispatch('loadUnits')
      } catch (err) {
      } finally {
        commit('toggleLoading', false)
      }
    }
  }
})

export default store
