const typeList = [
  { id: 1, name: 'food' },
  { id: 2, name: 'medicine' }
]

const units = [
  { id: 1, typeId: 1, amount: 200, date: new Date(2022, 7, 29) },
  { id: 2, typeId: 2, amount: 400, date: new Date(2022, 7, 29) },
  { id: 3, typeId: 1, amount: 500, date: new Date(2022, 7, 29) },
  { id: 4, typeId: 1, amount: 100, date: new Date(2022, 7, 29) }
]

export default {
  createType (typeName) {
    const newType = { id: typeList.length + 1, name: typeName }
    typeList.push(newType)
    return new Promise((resolve) => {
      setTimeout(() => resolve(newType), 1000)
    })
  },

  loadTypes () {
    return new Promise((resolve) => {
      setTimeout(() => resolve(typeList), 2000)
    })
  },

  createUnit (newUnit) {
    newUnit.id = units.length + 1
    units.push(newUnit)
    return new Promise((resolve) => {
      setTimeout(() => resolve(newUnit), 1500)
    })
  },

  loadUnits (filter) {
    return new Promise((resolve) => {
      setTimeout(() => resolve(units.filter(unit => filter ? unit.date <= filter.dateFrom && unit.date >= filter.dateTo : true)), 2000)
    })
  }

}
